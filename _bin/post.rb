#!/usr/bin/env ruby

# Uso:
#   _bin/post.rb https://archive.org/details/desdeelbarro_programa038
#
#   Descarga la página, la procesa por usando nokogiri y genera el post
#   correspondiente

require 'net/http'
require 'date'
require 'nokogiri'
require 'httparty'

uri = URI ARGV.shift
doc = Nokogiri::HTML(Net::HTTP.get(uri))

date_published = doc.css('span[itemprop="datePublished"]').first.content
identifier = doc.css('span[itemprop="identifier"]').first.content
keywords = doc.css('dd[itemprop="keywords"] a').map(&:content)
mp3_file = doc.css('.item-download-options .format-summary')
  .find { |node| node.content =~ /VBR MP3/ }
  .attr('href')
file = URI.join uri, mp3_file

lenght = HTTParty.head(file)['content-length'].to_i
date = Date.parse date_published
number = identifier[/\d+/].to_i
body = doc.css('[itemprop="description"]').first.content

filename = p "_posts/#{ date }-programa-#{ number }.md"

if File.exist? filename
  warn "Already there"
  exit 1
end

headers = {
  'tags' => keywords,
  'file' => file.to_s,
  'lenght' => lenght,
}

template = <<TEMPLATE
#{ headers.to_yaml }
---

#{ body }
TEMPLATE

File.write filename, template
