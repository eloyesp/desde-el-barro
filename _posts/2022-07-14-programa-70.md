---
tags:
- musica
- militancia
- santa fe
- territorio
- cultura
- arte
- radio
- radio comunitaria
- literatura
- escritores
- escritoras
- emilia cersofio
- emi cersofio
- emi cersofio y su conjunto
- rock
- alejandra bosch
- poesía
- '2012'
- el fuego de la semilla
- día
file: https://archive.org/download/desdeelbarro_programa070/programa070.mp3
lenght: 82707158
chapters:
- title: Presentación
  start: '00:00:00'
- title: La consigna
  start: '00:12:30'
- title: El tema de la semana
  start: '00:26:36'
- title: El mejor disco de 2012
  start: '00:48:00'
- title: Espacio literario
  start: '00:57:42'
- title: Entrevista Musical
  start: '01:09:30'
- title: Cierre
  start: '01:51:52'

---

Programa 70 de Desde el Barro. Décimo sexto de 2022. Invitades: Emi Cersofio. Espacio literario: Alejandra Bosch. Mejor disco del año 2012.

