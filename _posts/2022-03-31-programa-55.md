---
tags:
- musica
- militancia
- santa fe
- territorio
- cultura
- arte
- radio
- radio comunitaria
- literatura
- escritores
- escritoras
- Juanito Laguna
- Luciano Candioti
- Lucho Candioti
- hambre
- pobreza
- marginalidad
- hogar
- talleres
- giovi novello
- punk
- rock
- disidente
- trans
- varones trans
- disidencias
- billy raven
file: https://archive.org/download/desdeelbarro_programa055/programa055%20editado.mp3
lenght: 80424519
chapters:
  - start: "00:00"
    title: Presentación
  - start: "14:00"
    title: La consigna
  - start: "23:14"
    title: Entrevista Militancia
  - start: "01:00:38"
    title: Espacio literario
  - start: "01:06:39"
    title: Tema de la semana
  - start: "01:21:12"
    title: Entrevista Musical
  - start: "01:40:03"
    title: Cierre

---

Programa 55 de Desde el Barro. Primero de 2022. Invitades: Asociación Civil Juanito Laguna y Giovi Novello. Texto literario: Billy Raven.

