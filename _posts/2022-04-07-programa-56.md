---
tags:
- musica
- militancia
- santa fe
- territorio
- cultura
- arte
- radio
- radio comunitaria
- literatura
- escritores
- escritoras
- Coordinadora de la costa
- Diego Segala
- Arroyo Leyes
- Srta Miraflores
- Luisina Forzani
file: https://res.cloudinary.com/eloyesp/video/upload/v1649385573/desde_el_barro/programa_056_uqbkeg.mp3
lenght: 58291344
chapters:
  - start: "00:00"
    title: Presentación
  - start: "9:38"
    title: La consigna
  - start: "21:38"
    title: Entrevista Militancia
  - start: "00:50:05"
    title: Intermedio
  - start: "00:56:17"
    title: Tema de la semana
  - start: "01:06:39"
    title: Entrevista Musical
  - start: "01:42:13"
    title: Cierre

---

Programa 56 de Desde el Barro. Segundo de 2022. Invitades: Coordinadora de la
Costa y Srta Miraflores. Programa sin Magui.
